let faveFood = 'Egg Dishes'
console.log(faveFood)

let var1 = 150
let var2 = 9
console.log(var1+var2)

let var3 = 100
let var4 = 90
console.log(var3*var4)

let isActive = true

let faveResto = ["Pizza Hut", "Dairy Queen", "Burger King", "KFC", "Subway"]
console.log(faveResto)

let faveArtist = {
	firstName: 'Seok-jin',
	lastName: 'Kim',
	stageName: 'JIN',
	birthday: 'December 4, 1992',
	age: '29',
	bestAlbum: 'Love Yourself: Answer',
	bestSong: 'Abyss',
	isActive: true
};

console.log(faveArtist)

function division(divElement1, divElement2){
	return `${divElement1}` / `${divElement2}`
};

let quotient = division(21, 7)
console.log("The result of the division is: " + quotient)